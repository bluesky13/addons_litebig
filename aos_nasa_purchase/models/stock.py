# -*- coding: utf-8 -*- 
# Part of Odoo. See LICENSE file for full copyright and licensing details. 
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from datetime import datetime 
from odoo.tools.misc import formatLang, format_date
from odoo.tools.float_utils import float_compare, float_is_zero, float_round
from odoo.exceptions import UserError

# class StockPickingType(models.Model): 
#     _inherit = 'stock.picking.type'
#     
#     fold = fields.Boolean(string='Folded in Kanban',
#         help='This stage is folded in the kanban view when there are no records in that stage to display.')

class StockInventory(models.Model):
    _inherit = 'stock.inventory'
    
    def _action_done(self):
        negative = next((line for line in self.mapped('line_ids') if line.product_qty < 0 and line.product_qty != line.theoretical_qty), False)
        #if negative:
        #    raise UserError(_('You cannot set a negative product quantity in an inventory line:\n\t%s - qty: %s') % (negative.product_id.name, negative.product_qty))
        self.action_check()
        self.write({'state': 'done'})
        self.post_inventory()
        return True

class StockProductionLotGroup(models.Model):
    _name = 'stock.production.lot.group'
    _inherit = ['mail.thread','mail.activity.mixin']
    _description = 'SN Group'
    
    name = fields.Char(
        'Group Lot/Serial Number', default=lambda self: self.env['ir.sequence'].next_by_code('stock.lot.group.serial'),
        required=True, help="Group Unique Lot/Serial Number")
    ref = fields.Char('Internal Reference', help="Internal reference number in case it differs from the manufacturer's lot/serial number")
    lot_ids = fields.One2many('stock.production.lot', 'group_id', 'Lots', readonly=True)
    
    _sql_constraints = [
        ('name_ref_uniq', 'unique (name)', 'The group of serial number must be unique !'),
    ]
    
class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'
    
    group_id = fields.Many2one('stock.production.lot.group', string='Group SN')
    
class StockPicking(models.Model): 
    _inherit = 'stock.picking'
    
    active = fields.Boolean(
        'Active', default=True,
        help="If unchecked, it will allow you to hide the picking without removing it.") 
    is_return = fields.Boolean('Is Return')
    #show_range_qty = fields.Boolean('Show Range Qty', default=True)
    
    @api.multi
    def button_validate(self):
        self.ensure_one()
        if not self.move_lines and not self.move_line_ids:
            raise UserError(_('Please add some items to move.'))

        # If no lots when needed, raise error
        picking_type = self.picking_type_id
        precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        no_quantities_done = all(float_is_zero(move_line.qty_done, precision_digits=precision_digits) for move_line in self.move_line_ids.filtered(lambda m: m.state not in ('done', 'cancel')))
        no_reserved_quantities = all(float_is_zero(move_line.product_qty, precision_rounding=move_line.product_uom_id.rounding) for move_line in self.move_line_ids)
        if no_reserved_quantities and no_quantities_done:
            raise UserError(_('You cannot validate a transfer if no quantites are reserved nor done. To force the transfer, switch in edit more and encode the done quantities.'))

        if picking_type.use_create_lots or picking_type.use_existing_lots:
            lines_to_check = self.move_line_ids
            if not no_quantities_done:
                lines_to_check = lines_to_check.filtered(
                    lambda line: float_compare(line.qty_done, 0,
                                               precision_rounding=line.product_uom_id.rounding)
                )

            for line in lines_to_check:
                product = line.product_id
                if product and product.tracking != 'none':
                    if not line.lot_name and not line.lot_id:
                        raise UserError(_('You need to supply a Lot/Serial number for product %s.') % product.display_name)

        if no_quantities_done:
            view = self.env.ref('stock.view_immediate_transfer')
            wiz = self.env['stock.immediate.transfer'].create({'pick_ids': [(4, self.id)]})
            return {
                'name': _('Immediate Transfer?'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.immediate.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        if self._get_overprocessed_stock_moves() and not self._context.get('skip_overprocessed_check'):
            view = self.env.ref('stock.view_overprocessed_transfer')
            wiz = self.env['stock.overprocessed.transfer'].create({'picking_id': self.id})
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.overprocessed.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        # Check backorder should check for other barcodes
        if self._check_backorder():
            return self.action_generate_backorder_wizard()
        self.action_done()
        return
    
class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'
    
    
    @api.depends('location_id', 'location_dest_id', 'partner_id', 'state')
    def _compute_display_location(self):
        for mline in self:
            if mline.partner_id and mline.location_id.usage in ('customer','supplier'):
                mline.display_location_id = mline.partner_id.name
            else:
                mline.display_location_id = mline.location_id.display_name
            if mline.partner_id and mline.location_dest_id.usage in ('customer','supplier'):
                mline.display_location_dest_id = mline.partner_id.name
            else:
                mline.display_location_dest_id = mline.location_dest_id.display_name
            mline.group_id = mline.picking_id and mline.picking_id.group_id and mline.picking_id.group_id.name \
                or mline.picking_id and mline.picking_id.origin or ''
            #mline.display_location_dest_id = mline.partner_id and mline.partner_id.name if mline.location_dest_id.usage in ('customer','supplier') else mline.location_dest_id.display_name
            
    
    group_lot_id = fields.Many2one('stock.production.lot.group', string='Group Lot/SN', related='lot_id.group_id')
    location_id = fields.Many2one('stock.location', 'Source', required=True)
    location_dest_id = fields.Many2one('stock.location', 'Destination', required=True)    
    display_location_id = fields.Char('From', compute='_compute_display_location', store=False)
    display_location_dest_id = fields.Char('To', compute='_compute_display_location', store=False)
    group_id = fields.Char('Reference SO/PO', compute='_compute_display_location', store=False)
    #reference = fields.Char(string='Reference Warehouse', related='move_id.reference', store=True, related_sudo=False, readonly=False)
    
class StockQuant(models.Model):
    _inherit ='stock.quant'
    
    @api.model
    def _update_reserved_quantity(self, product_id, location_id, quantity, lot_id=None, package_id=None, owner_id=None, strict=False):
        """ Increase the reserved quantity, i.e. increase `reserved_quantity` for the set of quants
        sharing the combination of `product_id, location_id` if `strict` is set to False or sharing
        the *exact same characteristics* otherwise. Typically, this method is called when reserving
        a move or updating a reserved move line. When reserving a chained move, the strict flag
        should be enabled (to reserve exactly what was brought). When the move is MTS,it could take
        anything from the stock, so we disable the flag. When editing a move line, we naturally
        enable the flag, to reflect the reservation according to the edition.

        :return: a list of tuples (quant, quantity_reserved) showing on which quant the reservation
            was done and how much the system was able to reserve on it
        """
        self = self.sudo()
        rounding = product_id.uom_id.rounding
        quants = self._gather(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
        reserved_quants = []

        if float_compare(quantity, 0, precision_rounding=rounding) > 0:
            # if we want to reserve
            available_quantity = self._get_available_quantity(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
            #if float_compare(quantity, available_quantity, precision_rounding=rounding) > 0:
            #    raise UserError(_('It is not possible to reserve more products of %s than you have in stock.') % product_id.display_name)
        elif float_compare(quantity, 0, precision_rounding=rounding) < 0:
            # if we want to unreserve
            available_quantity = sum(quants.mapped('reserved_quantity'))
            #if float_compare(abs(quantity), available_quantity, precision_rounding=rounding) > 0:
            #    raise UserError(_('It is not possible to unreserve more products of %s than you have in stock.') % product_id.display_name)
        else:
            return reserved_quants

        for quant in quants:
            if float_compare(quantity, 0, precision_rounding=rounding) > 0:
                max_quantity_on_quant = quant.quantity - quant.reserved_quantity
                if float_compare(max_quantity_on_quant, 0, precision_rounding=rounding) <= 0:
                    continue
                max_quantity_on_quant = min(max_quantity_on_quant, quantity)
                quant.reserved_quantity += max_quantity_on_quant
                reserved_quants.append((quant, max_quantity_on_quant))
                quantity -= max_quantity_on_quant
                available_quantity -= max_quantity_on_quant
            else:
                max_quantity_on_quant = min(quant.reserved_quantity, abs(quantity))
                quant.reserved_quantity -= max_quantity_on_quant
                reserved_quants.append((quant, -max_quantity_on_quant))
                quantity += max_quantity_on_quant
                available_quantity += max_quantity_on_quant

            if float_is_zero(quantity, precision_rounding=rounding) or float_is_zero(available_quantity, precision_rounding=rounding):
                break
        return reserved_quants