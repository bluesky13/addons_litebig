# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _


class AccountDotmatrixReceipt(models.TransientModel):
    _name = 'account.dotmatrix.receipt'

    name = fields.Char("Test")


class AccountInvoice(models.Model):
    _inherit = "account.invoice"
 
    @api.multi
    def print_new_receipt(self):
        templ = self.env['mail.template'].search([('name', '=', 'Dot Matrix AI')])
        if templ:
            data = templ._render_template(templ[0].body_html, 'account.invoice', self.id)
            view_id = self.env.ref('invoice_dotmatrix_printer.account_dotmatrix_receipt_form').id
            res = {
            	"name":data,
            }
            wizard_id = self.env['account.dotmatrix.receipt'].create(res)
            context = self._context.copy()
            return {
                'name':'Print Receipt',
                'view_type':'form',
                'view_mode':'tree',
                'views' : [(view_id,'form')],
                'res_model':'account.dotmatrix.receipt',
                'view_id':view_id,
                'type':'ir.actions.act_window',
                'res_id':wizard_id.id,
                'target':'new',
                'context':context,

            }






