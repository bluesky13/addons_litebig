from odoo import api, fields, models, _
from datetime import datetime, date, time
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class purchase(models.Model):
    _name = 'purchase.order'
    _inherit = 'purchase.order'

    @api.multi
    def generate_printer_data_report(self):
        tpl = self.env['mail.template'].search([('name', '=', 'Dot Matrix PO litebig')])
        data = tpl._render_template(tpl.body_html, 'purchase.order', self.id, post_process=False)
        data = data.replace('&#34;','"')
        self.printer_data = data

    def get_company(self):
        return {
            'name': self.company_id.name or '-',
            'state': self.company_id.state_id.name or '-',
            'street': self.company_id.street or '-',
            'city': self.company_id.city or '-',
            'zip': self.company_id.zip or '-',
            'country': self.company_id.country_id.name or '-',
            'phone': self.company_id.phone or '-',
            'vat': self.company_id.vat or '-',
            'npwp': self.company_id.npwp or '-',
        }

    def get_partner(self):
        return {
            'name': self.partner_id.name or '-',
            'state': self.partner_id.state_id.name or '-',
            'street': self.partner_id.street or '-',
            'city': self.partner_id.city or '-',
            'zip': self.partner_id.zip or '-',
            'country': self.partner_id.country_id.name or '-',
            'phone': self.partner_id.phone or '-',
            'vat': self.partner_id.vat or '-',
        }

    def get_data_lines(self):
        line_inv = []
        for line in self.order_line:
            line_inv.append({
                'product_code': line.product_id.default_code or '',
                'product_name': line.product_id.name or '',
                'name': line.name or '',
                'poin_ok': line.poin_ok,
                'product_uom_qty': line.product_uom_qty or 0.00,
                'qty_invoiced': line.qty_invoiced or 0.00,
                'price_unit': line.price_unit or 0.00,
                'price_subtotal': line.price_subtotal or 0,
                'point_total': line.point_total or 0,
            })
        return line_inv

    def get_date_time(self):
        date_order = self.date_order
        return {
            'date_order': date_order.strftime('%Y-%m-%d'),
            'time_order': date_order.strftime('%H:%M:%S'),
        }

    def get_total_bv(self):
        subtotal = 0
        for line in self.order_line:
            if line.poin_ok == True:
                subtotal =+ line.point_total
        return subtotal

    def get_total_harga_poin(self):
        subtotal = 0
        for line in self.order_line:
            if line.poin_ok == True:
                subtotal =+ line.price_subtotal
        return subtotal

    def get_total_harga_nonpoin(self):
        subtotal = 0
        for line in self.order_line:
            if line.poin_ok == False:
                subtotal =+ line.price_subtotal
        return subtotal

    def get_invoice_no(self):
        no_invoice = ""
        for line in self.invoice_ids:
            no_invoice = line.number
        return no_invoice