from odoo import api, fields, models, _
from datetime import datetime, date, time
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class sale(models.Model):
    _name = 'sale.order'
    _inherit = 'sale.order'

    @api.multi
    def generate_printer_data(self):
        tpl = self.env['mail.template'].search([('name', '=', 'Dot Matrix SO litebig')])
        data = tpl._render_template(tpl.body_html, 'sale.order', self.id, post_process=False)
        data = data.replace('&#34;','"')
        self.printer_data = data

    def get_company(self):
        return {
            'name': self.company_id.name or '-',
            'state': self.company_id.state_id.name or '-',
            'street': self.company_id.street or '-',
            'city': self.company_id.city or '-',
            'zip': self.company_id.zip or '-',
            'country': self.company_id.country_id.name or '-',
            'phone': self.company_id.phone or '-',
            'vat': self.company_id.vat or '-',
            'npwp': self.company_id.npwp or '-',
        }

    def get_data_lines(self):
        line_inv = []
        for line in self.order_line:
            line_inv.append({
                'product_code': line.product_id.default_code or '',
                'product_name': line.product_id.name or '',
                'name': line.name or '',
                'poin_ok': line.poin_ok,
                'product_uom_qty': line.product_uom_qty or 0.00,
                'qty_invoiced': line.qty_invoiced or 0.00,
                'price_unit': line.price_unit or 0.00,
                'price_subtotal': line.price_subtotal or 0,
                'point_total_bv': line.point_total_bv or 0,
            })
        return line_inv

    def get_date_time(self):
        if (self.state == 'done'):
            confirmation_date = self.confirmation_date
            return {
                'confirmation_date': confirmation_date.strftime('%Y-%m-%d'),
                'confirmation_time': confirmation_date.strftime('%H:%M:%S'),
            }
        else :
            validity_date = self.validity_date
            return {
                'confirmation_date': validity_date.strftime('%Y-%m-%d'),
                'confirmation_time': validity_date.strftime('%H:%M:%S'),
            }

    def get_total_bv(self):
        subtotal = 0
        for line in self.order_line:
            if line.poin_ok == True:
                subtotal =+ line.point_total_bv
        return subtotal

    def get_total_harga_poin(self):
        subtotal = 0
        for line in self.order_line:
            if line.poin_ok == True:
                subtotal =+ line.price_subtotal
        return subtotal

    def get_total_harga_nonpoin(self):
        subtotal = 0
        for line in self.order_line:
            if line.poin_ok == False:
                subtotal =+ line.price_subtotal
        return subtotal

    def get_invoice_no(self):
        no_invoice = ""
        for line in self.invoice_ids:
            no_invoice = line.number
        return no_invoice







 