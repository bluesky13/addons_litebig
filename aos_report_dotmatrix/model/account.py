from odoo import api, fields, models, _
from datetime import datetime, date, time
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class accountInvoice(models.Model):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    @api.multi
    def generate_printer_data_report(self):
        tpl = self.env['mail.template'].search([('name', '=', 'Dot Matrix Invoice litebig')])
        data = tpl._render_template(tpl.body_html, 'account.invoice', self.id, post_process=False)
        data = data.replace('&#34;','"')
        self.printer_data = data

    def get_data(self):
        return {
            'number': self.number or 'Draft Invoice',
            'date_invoice': self.date_invoice or '-',
        }

    def get_company(self):
        return {
            'name': self.company_id.name or '-',
            'state': self.company_id.state_id.name or '-',
            'street': self.company_id.street or '-',
            'city': self.company_id.city or '-',
            'zip': self.company_id.zip or '-',
            'country': self.company_id.country_id.name or '-',
            'phone': self.company_id.phone or '-',
            'vat': self.company_id.vat or '-',
            'npwp': self.company_id.npwp or '-',
        }

    def get_partner(self):
        return {
            'name': self.partner_id.name or '-',
            'state': self.partner_id.state_id.name or '-',
            'street': self.partner_id.street or '-',
            'city': self.partner_id.city or '-',
            'zip': self.partner_id.zip or '-',
            'country': self.partner_id.country_id.name or '-',
            'phone': self.partner_id.phone or '-',
            'vat': self.partner_id.vat or '-',
            # 'npwp': self.partner_id.npwp or '-',
        }

    def get_data_lines(self):
        line_inv = []
        for line in self.invoice_line_ids:
            line_inv.append({
                'product_code': line.product_id.default_code or '',
                'product_name': line.product_id.name or '',
                'name': line.name or '',
                'poin_ok': line.poin_ok,
                'quantity': line.quantity or 0.00,
                'price_unit': line.price_unit or 0.00,
                'price_subtotal': line.price_subtotal or 0,
                'point_total': line.point_total or 0,
            })
        return line_inv

    def get_total_bv(self):
        subtotal = 0
        for line in self.invoice_line_ids:
            if line.poin_ok == True:
                subtotal =+ line.point_total
        return subtotal

    def get_total_harga_poin(self):
        subtotal = 0
        for line in self.invoice_line_ids:
            if line.poin_ok == True:
                subtotal =+ line.price_subtotal
        return subtotal

    def get_total_harga_nonpoin(self):
        subtotal = 0
        for line in self.invoice_line_ids:
            if line.poin_ok == False:
                subtotal =+ line.price_subtotal
        return subtotal

class accountPayment(models.Model):
    _name = 'account.payment'
    _inherit = 'account.payment'

    printer_data = fields.Text(string="Printer Data", required=False, readonly=False)

    @api.multi
    def generate_printer_data_report(self):
        tpl = self.env['mail.template'].search([('name', '=', 'Dot Matrix Payment litebig')])
        data = tpl._render_template(tpl.body_html, 'account.payment', self.id, post_process=False)
        data = data.replace('&#34;','"')
        self.printer_data = data

    def get_data(self):
        return {
            'no_invoice': self.communication or '-',
            'date_invoice': str(self.date_invoice) or '-',
            'no_faktur': self.no_invoice or '-',
            'ttl_faktur': self.total_penjualan or 0.00,
            'stockist': self.partner_id.customer_ref or '-',
            'bank1': self.journal_nonbank_id1.name or '-',
            'bank2': self.journal_nonbank_id2.name or '-',
            'bank3': self.journal_nonbank_id3.name or '-',
            'bank4': self.journal_nonbank_id4.name or '-',
            'bank5': self.journal_nonbank_id5.name or '-',
            'bank6': self.payment_method_id.name or '-',
            'amount1': self.amount1 or 0.00,
            'amount2': self.amount2 or 0.00,
            'amount3': self.amount3 or 0.00,
            'amount4': self.amount4 or 0.00,
            'amount5': self.amount5 or 0.00,
            'amount': self.amount or 0.00,
            'total_transfer_amount': self.total_transfer_amount or 0.00,
        }

    def get_company(self):
        return {
            'name': self.company_id.name or '-',
            'state': self.company_id.state_id.name or '-',
            'street': self.company_id.street or '-',
            'city': self.company_id.city or '-',
            'zip': self.company_id.zip or '-',
            'country': self.company_id.country_id.name or '-',
            'phone': self.company_id.phone or '-',
            'vat': self.company_id.vat or '-',
        }

    def get_partner(self):
        return {
            'name': self.partner_id.name or '-',
            'state': self.partner_id.state_id.name or '-',
            'street': self.partner_id.street or '-',
            'city': self.partner_id.city or '-',
            'zip': self.partner_id.zip or '-',
            'country': self.partner_id.country_id.name or '-',
            'phone': self.partner_id.phone or '-',
            'vat': self.partner_id.vat or '-',
            # 'npwp': self.partner_id.npwp or '-',
        }

    def get_data_lines(self):
        line_inv = []
        for line in self.invoice_line_ids:
            line_inv.append({
                'product_code': line.product_id.default_code or '',
                'product_name': line.product_id.name or '',
                'name': line.name or '',
                'poin_ok': line.poin_ok,
                'quantity': line.quantity or 0.00,
                'price_unit': line.price_unit or 0.00,
                'price_subtotal': line.price_subtotal or 0,
                'point_total': line.point_total or 0,
            })
        return line_inv