from odoo import api, fields, models, _
from datetime import datetime, date, time
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    _name = 'res.company'
    _inherit = 'res.company'

    npwp = fields.Char('NPWP', default='00.000.000.0-000.000', help='Nomor Pokok Wajib Pajak')

    @api.onchange('npwp')
    def onchange_npwp(self):
        res = {}
        vals = {}
        if not self.npwp:
            return
        elif len(self.npwp)==20:
            self.npwp = self.npwp
        elif len(self.npwp)==15:
            formatted_npwp = self.npwp[:2]+'.'+self.npwp[2:5]+'.'+self.npwp[5:8]+'.'+self.npwp[8:9]+'-'+self.npwp[9:12]+'.'+self.npwp[12:15]
            self.npwp = formatted_npwp
        else:
            warning = {
                'title': _('Warning'),
                'message': _('Wrong Format must 15 digit'),
            }
            return {'warning': warning, 'value' : {'npwp' : False}}
        return res