from odoo import api, fields, models, _
from datetime import datetime, date, time
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _name = 'stock.picking'
    _inherit = 'stock.picking'

    @api.multi
    def generate_printer_data_receipts(self):
        tpl = self.env['mail.template'].search([('name', '=', 'Dot Matrix Receipts Stock litebig')])
        data = tpl._render_template(tpl.body_html, 'stock.picking', self.id, post_process=False)
        data = data.replace('&#34;','"')
        self.printer_data = data

    @api.multi
    def generate_printer_data_do(self):
        tpl = self.env['mail.template'].search([('name', '=', 'Dot Matrix DO Stock litebig')])
        data = tpl._render_template(tpl.body_html, 'stock.picking', self.id, post_process=False)
        data = data.replace('&#34;','"')
        self.printer_data = data

    def get_company(self):
        return {
            'name': self.company_id.name or '-',
            'state': self.company_id.state_id.name or '-',
            'street': self.company_id.street or '-',
            'city': self.company_id.city or '-',
            'zip': self.company_id.zip or '-',
            'country': self.company_id.country_id.name or '-',
            'phone': self.company_id.phone or '-',
            'vat': self.company_id.vat or '-',
            'npwp': self.company_id.npwp or '-',
        }

    def get_partner(self):
        return {
            'name': self.partner_id.name or '-',
            'state': self.partner_id.state_id.name or '-',
            'street': self.partner_id.street or '-',
            'city': self.partner_id.city or '-',
            'zip': self.partner_id.zip or '-',
            'country': self.partner_id.country_id.name or '-',
            'phone': self.partner_id.phone or '-',
            'vat': self.partner_id.vat or '-',
            'npwp': self.partner_id.npwp or '-',
        }

    def get_data_lines(self):
        line_inv = []
        for line in self.move_ids_without_package:
            line_inv.append({
                'product_code': line.product_id.default_code or '',
                'product_name': line.product_id.name or '',
                'product_uom_qty': line.product_uom_qty or 0.00,
                'quantity_done': line.quantity_done or 0.00,
                'weight': line.product_id.weight or 0.00,
            })
        return line_inv

    def get_total_qty(self):
        subtotal = 0
        for line in self.move_ids_without_package:
            subtotal =+ line.quantity_done
        return subtotal

    def get_date_done(self):
        return str(self.date_done.strftime('%Y/%m/%d'))







 