{
	"name": "Report Dot Matrix Printer",
	"version": "12.0.1.0.0",
	"depends": ["vit_dotmatrix", "sale", "purchase","base"],
	"author": "Alphasoft",
	"category": "Utilities",
	'website': 'https://www.alphasoft.co.id',
	'summary': 'Report Dot Matrix',
    'images':  ['images/main_screenshot.png'],
	"description": """
	v.1.0 \n
    Author : APR \n
    1. inherit modul vit_dotmatrix\n
	""",
	"data": [
		"view/purchase.xml",
		"view/invoice.xml",
		"view/stock_picking.xml",
		"view/account_payment.xml",
		"view/res_company.xml",
		"data/templates.xml",
	],
	"installable": True,
	"auto_install": False,
    "application": False,
}