from odoo import models, api, _
from odoo.exceptions import UserError

# class AccountInvoiceToDraft(models.TransientModel):
#     """
#     This wizard will cancel the all the selected invoices.
#     If in the journal, the option allow cancelling entry is not selected then it will give warning message.
#     """
# 
#     _name = "account.invoice.to.draft"
#     _description = "Set to Draft the Selected Invoices"
# 
#     @api.multi
#     def invoice_to_draft(self):
#         context = dict(self._context or {})
#         active_ids = context.get('active_ids', []) or []
# 
#         for record in self.env['account.invoice'].browse(active_ids):
#             if record.state in ('draft', 'paid'):
#                 raise UserError(_("Selected invoice(s) cannot be draft as they are already in 'Draft' or 'Done' state."))
#             record.action_invoice_draft()
#         return {'type': 'ir.actions.act_window_close'}
#     
class ConfirmSaleOrder(models.TransientModel):
    _name = "confirm.sale.order"
    _description = "Confirm Sale Order"

    @api.multi
    def confirm(self):
        context = dict(self._context or {})
        quotations = self.env['sale.order'].browse(context.get('active_ids'))
        sale_to_confirm = self.env['sale.order']
        for sale in quotations:
            if sale.state == 'draft':
                sale_to_confirm += sale
        if not sale_to_confirm:
            raise UserError(_('There is no quotations items in draft state to confirm.'))
        sale_to_confirm.action_confirm()
        return {'type': 'ir.actions.act_window_close'}
