from odoo import models, api, _
from odoo.exceptions import UserError

class ActionStockPicking(models.TransientModel):
    _name = "action.stock.picking"
    _description = "Action Stock Picking"

#     @api.multi
#     def confirm(self):
#         context = dict(self._context or {})
#         quotations = self.env['sale.order'].browse(context.get('active_ids'))
#         sale_to_confirm = self.env['sale.order']
#         for sale in quotations:
#             if sale.state == 'draft':
#                 sale_to_confirm += sale
#         if not sale_to_confirm:
#             raise UserError(_('There is no quotations items in draft state to confirm.'))
#         sale_to_confirm.action_confirm()
#         return {'type': 'ir.actions.act_window_close'}
    
    @api.multi
    def action_confirm(self):
        context = dict(self._context or {})
        draft_pickings = self.env['stock.picking'].browse(context.get('active_ids'))
        for pick in draft_pickings:
            pick.action_confirm()
        return {'type': 'ir.actions.act_window_close'}
    
    @api.multi
    def action_assign(self):
        context = dict(self._context or {})
        waiting_pickings = self.env['stock.picking'].browse(context.get('active_ids'))
        for pick in waiting_pickings:
            pick.action_assign()
        return {'type': 'ir.actions.act_window_close'}
    
    @api.multi
    def button_validate(self):
        context = dict(self._context or {})
        ready_pickings = self.env['stock.picking'].browse(context.get('active_ids'))
        for pick in ready_pickings:
            pick.button_validate()
        return {'type': 'ir.actions.act_window_close'}
